#!/bin/python
# -*- coding: utf-8 -*-
# __author__ = sunsn
# __datetime__ = 2020/12/31 14:23
"""
装饰器模块
"""
import time
import logging
from functools import wraps

from .excptions import MaxRetryError

logger = logging.getLogger(__name__)


def retry(max_tries: int = 3, interval: int = 5):
    @wraps
    def inner(func, *args, **kwargs):
        for i in range(1, max_tries + 1):
            try:
                res = func(*args, **kwargs)
            except Exception as e:
                logger.error(f"获取数据异常，错误原因： {e}")
                continue
            else:
                if res is not None:
                    return res

            logger.info(f"第{i}次，取数失败")
            time.sleep(interval)

        raise MaxRetryError(f"重试次数已达上线：{max_tries}次")

    return inner
