#!/bin/python
# -*- coding: utf-8 -*-
# __author__ = sunsn
# __datetime__ = 2020/12/31 14:21
"""
模块注释
"""

import pathlib

BASE_DIR = pathlib.Path(__file__).parent.parent
DATA_DIR = BASE_DIR / "data"


SONAR_FILEPATH = DATA_DIR / "sonar.csv"
ABALONE_FILEPATH = DATA_DIR / "abalone.csv"
RED_WINE_QUALITY_FILEPATH = DATA_DIR / "red_wine_quality.csv"
GLASS_FILEPATH = DATA_DIR / "glass.csv"

